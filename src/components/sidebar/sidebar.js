import "./sidebar.css";
import React, { Component } from "react";
import { Row, Col, ListGroup } from "react-bootstrap";
import {
  MdCalendarViewMonth,
  MdPerson,
  MdGroups,
  MdStickyNote2,
  MdOutlineAvTimer,
  MdSettings,
  MdOutlineHelpOutline,
} from "react-icons/md";

const icons = [
  MdCalendarViewMonth,
  MdPerson,
  MdGroups,
  MdStickyNote2,
  MdOutlineAvTimer,
  MdSettings,
  MdOutlineHelpOutline,
];

export default class Sidebar extends Component {
  state = {
    menuElements: [
      {
        name: "Обзор",
        icon: "<MdCalendarViewMonth/>",
        id: "menuEl1",
        active: true,
      },
      {
        name: "Преподаватели",
        icon: "<MdPerson/>",
        id: "menuEl2",
        active: false,
      },
      {
        name: "Группы",
        icon: "<MdGroups/>",
        id: "menuEl3",
        active: false,
      },
      {
        name: "Предметы",
        icon: "<MdStickyNote2/>",
        id: "menuEl4",
        active: false,
      },
      {
        name: "Управление",
        icon: "<MdOutlineAvTimer/>",
        id: "menuEl5",
        active: false,
      },
      {
        name: "Настройки",
        icon: "<MdSettings/>",
        id: "menuEl6",
        active: false,
      },
      {
        name: "Помощь",
        icon: "<MdOutlineHelpOutline/>",
        id: "menuEl7",
        active: false,
      },
    ],
  };

  setSection = (active) => {
    this.props.onSetSection(active);
  };

  makeActive = (id) => {
    this.setState(({ menuElements }) => {
      const idx = menuElements.findIndex((el) => el.id === id);
      const oldItem = menuElements[idx];
      const newItem = {
        ...oldItem,
        active: !oldItem["active"],
      };
      this.setSection(menuElements[idx].name);
      return {
        menuElements: [
          ...menuElements
            .slice(0, idx)
            .map((el) => Object.assign(el, { active: false })),
          newItem,
          ...menuElements
            .slice(idx + 1)
            .map((el) => Object.assign(el, { active: false })),
        ],
      };
    });
  };

  render() {
    return (
      <Col xs={4} md={2} className="sidebar">
        <Row>
          <Col className="LogoTitle">
            Архангельский Колледж Телекоммуникаций
          </Col>
        </Row>
        <Row className="Main">
          <ListGroup className="MainMenu" variant="flush">
            {this.state.menuElements.map((el, idx) => {
              const Icon = icons[idx];
              if (el.active === true)
                return (
                  <ListGroup.Item
                    key={el.id}
                    className="MenuItem Active"
                    onClick={() => this.makeActive(el.id)}
                  >
                    <Icon className="MenuItemIcon" />
                    {el.name}
                  </ListGroup.Item>
                );
              return (
                <ListGroup.Item
                  key={el.id}
                  className="MenuItem"
                  onClick={() => this.makeActive(el.id)}
                >
                  <Icon className="MenuItemIcon" />
                  {el.name}
                </ListGroup.Item>
              );
            })}
          </ListGroup>
        </Row>
      </Col>
    );
  }
}
