import "./header.css";
import React, { Component } from "react";
import { Row, Col, Image } from "react-bootstrap";
import avatar from "./img/1.jpg"; // relative path to image

export default class Header extends Component {
  render() {
    const { section, user } = this.props;
    return (
      <Row className="InformationBar">
        <Col xs={4} className="CurrentTitle">
          <div>
            <h4>{section}</h4>
          </div>
        </Col>
        <Col className="ProfileCard">
          <div>
            <a>{user}</a>
            <Image src={avatar} roundedCircle className="avatar" />
          </div>
        </Col>
      </Row>
    );
  }
}
