import "./teachers.css";
import React, { Component } from "react";
import { Row, Col, Image, Container, Badge, Dropdown } from "react-bootstrap";
import { MdAddToPhotos, MdSort, MdFilterAlt } from "react-icons/md";
import mask from "./img/1.jpg"; // relative path to image
import trump from "./img/2.jpg"; // relative path to image

export default class Teachers extends Component {
  render() {
    return (
      <Container fluid>
        <Row className="headerRow">
          <Col xs={4} className="title">
            <h4>Все Преподаватели</h4>
          </Col>
          <Col className="settings">
            <div>
              <Dropdown>
                <Dropdown.Toggle className="dropName" id="dropdown-basic">
                  <MdSort />
                  Сортировка
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item href="#/action-1">А-Я</Dropdown.Item>
                  <Dropdown.Item href="#/action-2">Я-А</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>

            <div>
              <MdFilterAlt />
              Фильтр
            </div>
          </Col>
        </Row>
        <Row className="teachers-info-row">
          <Col xs={5}>ФИО</Col>
          <Col xs={3}>Руководитель</Col>
          <Col xs={2}>Классное Рук.</Col>
          <Col xs={2}>Статус</Col>
        </Row>
        <Container fluid className="teachers-main">
          <Row className="teachers-row">
            <Col xs={5}>
              <Image src={trump} roundedCircle className="teacher-pic" />
              Цыганкова Маргарита Альбертовна
            </Col>
            <Col xs={3}>Топанов Александр Павлович</Col>
            <Col xs={2}>ПКС-91, Р-91</Col>
            <Col xs={2}>
              <h5>
                <Badge bg="success" pill>
                  Работает
                </Badge>
              </h5>
            </Col>
          </Row>
          <Row className="teachers-row">
            <Col xs={5}>
              <Image src={mask} roundedCircle className="teacher-pic" />
              Зубарев Александр Андреевич
            </Col>
            <Col xs={3}>Топанов Александр Павлович</Col>
            <Col xs={2}>ИБ-88</Col>
            <Col xs={2}>
              <h5>
                <Badge bg="danger" pill>
                  Больничный
                </Badge>
              </h5>
            </Col>
          </Row>
          <Row className="teachers-row">
            <Col xs={5}>
              <Image src={trump} roundedCircle className="teacher-pic" />
              Лукина Светлана Викторовна
            </Col>
            <Col xs={3}>Топанов Александр Павлович</Col>
            <Col xs={2}>ПКС-95</Col>
            <Col xs={2}>
              <h5>
                <Badge bg="warning" pill>
                  В отпуске
                </Badge>
              </h5>
            </Col>
          </Row>
          <Row className="teachers-row">
            <Col xs={5}>
              <Image src={mask} roundedCircle className="teacher-pic" />
              Живаев Илья Сергеевич
            </Col>
            <Col xs={3}>Топанов Александр Павлович</Col>
            <Col xs={2}>Р-01</Col>
            <Col xs={2}>
              <h5>
                <Badge bg="success" pill>
                  Работает
                </Badge>
              </h5>
            </Col>
          </Row>
        </Container>
      </Container>
    );
  }
}
