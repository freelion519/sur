import "./App.css";
import React, { Component } from "react";
import Sidebar from "../sidebar";
import Header from "../header";
import { Container, Row, Col } from "react-bootstrap";
import Teachers from "../content/teachers";
import Settings from "../content/settings";

export default class App extends Component {
  state = {
    currentSection: "Обзор",
    currentUser: "Львов Никита Александрович",
  };

  setSection = (value) => {
    this.setState({ currentSection: value });
  };

  render() {
    return (
      <Container fluid>
        <Row>
          <Sidebar onSetSection={this.setSection} />
          <Col className="mainContent">
            <Header
              section={this.state.currentSection}
              user={this.state.currentUser}
            />
            <Row className="content">
              <Col>
                <Settings />
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="footer"></Row>
      </Container>
    );
  }
}
